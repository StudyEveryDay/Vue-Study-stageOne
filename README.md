## Vue每日学习
Day:1 看基础文档，仔细去阅读了所有基础部分的文档，明天提交文档的总结-基础部分和部分练习的代码。
1. 视图层框架，实现数据绑定-我认为是一个数据驱动的框架
2. 模版通过绑定检测手段，当属性发生改变事后，触发相对应的处理逻辑。（v-bind）
3. 声明渲染 ===》 {{ message }} 在data中绑定message数据属性。
4. 语法绑定 ===》 {{ number + 1 }} {{ ok ? 'YES' : ’NO’}} {{ Js原生语句操作}}  <div v-bind:id=“‘list’-id”></div> =》字符串拼接
5. 上述Vue实例下的数据作用域会作为Javascript被解析。只能绑定单个表达式
6. v-bind:click  => :click    v-on:click => @click
7. v-bind用于绑定属性（id checked disable title name） 
8. v-on用于监听DOM事件 click keyup keydown
9. v-model 用于双向数据绑定。
10. vue废弃了events选项，一个组件绑定多个事件，组件会变得很长 

1. 修饰符--指出一个指定应该以特殊的方式绑定
2. .stop - 阻止冒泡 event.stopPropagation()
3. .prevent - 组织触发默认行为 event.preventDefault()
4. .capture - 添加事件监听器时使用capture模式
5. .self - 只当事件是从侦听器绑定元素本身出发时候才触发回调函数
6. .{keyCode | keyAlias}
7. .native -（新增） 监听组件根元素的原生事件


1. 组件通信：用过$dispatch和$broadcast，Vue2被废弃了。组件events也被废弃。
2. 父组件向子组件传递数据使用props，子组件禁止修改props属性，props不再支持双向绑定
3. 知道的解决方法子组件向父组件传递时间使用v-on，子组件使用$emit(’test’)触发事件，父组件使用v-on:test订阅事件。$emit(XXX) 命名需要（XX-XXX）。
4. v-on只能订阅子组件，不能订阅子组件中的子组件。

1. 生命周期==对比 vue1 - vue2
2. init          ==》 beforeCreate 组件实例刚被创建，组件属性计算之前如data属性
3. created       ==》created 组件实例创建完成，熟悉绑定，DOM未生成。
4. beforeCompile ==》beforeMount 模版编译挂挂载之前
5. compiled      ==》mounted 模版编译挂挂载之后
6. ready         ==》mounted 模版编译挂挂载之后(不保证组件已在document)
7.               ==》beforeUpdate 组件更新之前
8.               ==》updated 组件更新之后
9.               ==》activated -for keep-alive组件被激活时候调用
10.               ==》deactivated -for keep-alive组件被移除时候调用
11. attached 废弃
12. detached 废弃
13. beforeDestory ==》beforeDestory 组件销毁前调用
14. destroy       ==》destroy 组件销毁后调用


1. data总结
2. data
3. props
4. propsData（新增）
5. computed === 处理复杂的逻辑-》声明计算属性-》将计算属性渲染到模版中 含有getter setter（默认只有getter）
6. methods === 写方法，一些绑定方法在里边写
7. watch == 用来响应数据的变化，执行异步操作和开销的操作


1. DOM总结 — 没怎么用过
2. el
3. template
4. render(新增)

1. 指令总结
2. v-once（新增） 一次性地插值，数据改变插值内容不会更新。<span v-once>hello {{mes}}</span>
3. v-text <span>{{msg}}</span> ==> <span v-text="msg"></span>
4. v-html 更新innerHTML  <div v-html="msg"></div>不安全 在用户提交内容上容易遭受XSS攻击
5. 

1. class style绑定
2. v-bind:class <span v-bind:class="{ active: isActive}"></span> 取决于isActive是true false
   <span class="xxx" v-bind:class="{ active: isActive，}"></span> 并存class也可以将class写在data中，加载
	 <div v-bind:class="[class1,class2]" 封装在数组的类数组,class1 class2在data中
	 也可以用三元表达式来判断需要哪个类。
	 <div v-bind:class = "[isTrue ? trueClass: '',errorClass]" 
	 v-bind: style 绑定样式，其实是一个javascript对象。
	 <div v-bind:style="[style1,style2]” 可以将多个样式加载到一个元素上
	 当v-bind:style使用需要特定前缀的css属性时，如transform，Vue会自动侦测病添加—没用过

1. v-if 
2. v-if 绑定template v-if v-else v-else-if（新增） 三个搭配可以链式使用 
key的使用，可以避免加载删除元素，简单来说可以复用元素—见demo
会根据数据的状态进行变化，会将所渲染的template销毁和重建，属于需要时候才会局部编译的情况
1. v-show
2. 显示隐藏DOM 和display相似，是最简单的display

1. v-for对数组进行便利，数据量大的组件用的比较多。用于对一组数组进行渲染。
2. v-for="item in items” 可以用template表天渲染多个元素块
3. 也可以对一个对象进行迭代，遍历出多有键值对（可以添加索引）
4. v-for="n in 10" 遍历出1到10
5. v-for不能自动将数据传到组件里-当v-for和组件一起使用时候 可以使用props解决 有demo
6. key == track-by=$index 用于跟踪节点的身份
7. 变异方法： push() pop() shift() unshift() splice() sort() reserve() 数组的方法
8. 重塑数组：filter() concat() slice()
9. demo: demo.items = demo.items.filter(function(item){ return item.message.match(/Foo/)})

